import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import javafx.application.Application;
import javafx.stage.Stage;

public class CalculateRide extends Application{

	public static void main(String[] args) {
	
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		
		JFrame frame = new JFrame();
	    Container container = frame.getContentPane();

	    GridBagLayout gbl = new GridBagLayout();

	    container.setLayout(gbl);
	    
	    //create gridbag constraint object
	    GridBagConstraints gbc = new GridBagConstraints();
	    
	    //create components
	    JLabel lbl_heading = new JLabel("Airport Ride");
	    JLabel lbl_from = new JLabel("From:");
	    JLabel lbl_extra = new JLabel("Extra");
	    JLabel lbl_pets = new JLabel("Pets");
	    JLabel lbl_use407 = new JLabel("Use 407");
	    JLabel lbl_addTip = new JLabel("Add Tip?");
	    JLabel lbl_totalFair = new JLabel("The total fair is:");
	    lbl_totalFair.setVisible(false);
	    JLabel lbl_totalFairValue = new JLabel("");
	    lbl_totalFairValue.setVisible(false);
	    JTextField txt_field = new JTextField(15);
	    JCheckBox checkBox1 = new JCheckBox();
	    JCheckBox checkBox2 = new JCheckBox();
	    JCheckBox checkBox3 = new JCheckBox();
	    JCheckBox checkBox4 = new JCheckBox();
	    JButton button = new JButton("CALCULATE");
	    
	    //set label font
	   lbl_heading.setFont(new Font("Tahoma", Font.BOLD, 30));
	   lbl_from.setFont(new Font("Tahoma", Font.BOLD, 15));
	   lbl_extra.setFont(new Font("Tahoma", Font.BOLD, 15));
	   lbl_pets.setFont(new Font("Tahoma", Font.BOLD, 15));
	   lbl_use407.setFont(new Font("Tahoma", Font.BOLD, 15));
	   lbl_addTip.setFont(new Font("Tahoma", Font.BOLD, 15));
	   lbl_totalFair.setFont(new Font("Tahoma", Font.BOLD, 15));
	   lbl_totalFairValue.setFont(new Font("Tahoma", Font.BOLD, 15));

	   
	    gbc.gridwidth = 2; //span 2 columns
	    gbc.gridx = 0; //column 1
	    gbc.gridy = 0; //row 1
	    gbl.setConstraints(lbl_heading, gbc);
	    container.add(lbl_heading);

	    gbc.insets = new Insets(10,0,0,10);  //top padding
	    
	    gbc.gridwidth = 1;
	    gbc.gridx = 0;
	    gbc.gridy = 1;
	    gbc.anchor = GridBagConstraints.WEST; //left align
	    gbl.setConstraints(lbl_from, gbc);
	    container.add(lbl_from);
	    
	    gbc.insets = new Insets(0,0,0,0);
	    gbc.gridx = 1;
	    gbc.gridy = 1;
	    gbl.setConstraints(txt_field, gbc);
	    container.add(txt_field);

	    
	    gbc.insets = new Insets(10,0,0,0);
	    gbc.anchor = GridBagConstraints.WEST; //left align
	    gbc.gridx = 0;
	    gbc.gridy = 2;
	    gbl.setConstraints(checkBox1, gbc);
	    container.add(checkBox1);
	    
	    
	    gbc.gridx = 1;
	    gbc.gridy = 2;
	    gbc.anchor = GridBagConstraints.WEST;
	    gbl.setConstraints(lbl_extra, gbc);
	    container.add(lbl_extra);
	    
	    
	    gbc.gridx = 0;
	    gbc.gridy = 3;
	    gbl.setConstraints(checkBox2, gbc);
	    container.add(checkBox2);
	    
	    gbc.gridx = 1;
	    gbc.gridy = 3;
	    gbc.anchor = GridBagConstraints.WEST;
	    gbl.setConstraints(lbl_pets, gbc);
	    container.add(lbl_pets);
	    
	    gbc.gridx = 0;
	    gbc.gridy = 4;
	    gbl.setConstraints(checkBox3, gbc);
	    container.add(checkBox3);
	    
	    gbc.gridx = 1;
	    gbc.gridy = 4;
	    gbc.anchor = GridBagConstraints.WEST;
	    gbl.setConstraints(lbl_use407, gbc);
	    container.add(lbl_use407);
	    
	    gbc.gridx = 0;
	    gbc.gridy = 5;
	    gbl.setConstraints(checkBox4, gbc);
	    container.add(checkBox4);
	    
	    gbc.gridx = 1;
	    gbc.gridy = 5;
	    gbc.anchor = GridBagConstraints.WEST;
	    gbl.setConstraints(lbl_addTip, gbc);
	    container.add(lbl_addTip);
	    
	    gbc.insets = new Insets(10,0,0,0);
	    gbc.gridx = 0;
	    gbc.gridy = 6;
	    gbc.gridwidth = 2;
	    gbl.setConstraints(button, gbc);
	    container.add(button);
	    
	    gbc.gridwidth = 1; //reset to column 1
	    gbc.gridx = 0;
	    gbc.gridy = 7;
	    gbl.setConstraints(lbl_totalFair, gbc);
	    container.add(lbl_totalFair);
	    
	    gbc.gridx = 1;
	    gbc.gridy = 7;
	    gbc.anchor = GridBagConstraints.WEST;
	    //@gbc.insets = new Insets(0,0,0,10);
	    gbl.setConstraints(lbl_totalFairValue, gbc);
	    container.add(lbl_totalFairValue);
	    

	    frame.setPreferredSize(new Dimension(400, 350)); //set frame width and height
	    frame.pack();
	    frame.setVisible(true);
	    
	    button.addActionListener(new ActionListener() {

	        @Override
	        public void actionPerformed(ActionEvent e) {
	        	double extra_luggage = 0;
	        	double pets = 0;
	        	double use407 = 0;
	        	double tip = 0;
	        	double charges = 0;
	        	double baseFare = 0;
	        	double tax = 0;
	        	double totalPrice = 0;
	        	String from_location = txt_field.getText().toLowerCase();
	        	
	        	DecimalFormat df = new DecimalFormat("#.00");
	        	
	        	if(from_location.equals("cestar college"))
	        	{
	        		baseFare = 51 + 0.13;
	        	}
	        	else if(from_location.equals("brampton"))
	        	{
	        		baseFare = 38 + 0.13;
	        	}
	        	else {
	        		 
	        		lbl_totalFair.setVisible(true);
		        	lbl_totalFairValue.setText("Please enter correct location");
		        	lbl_totalFairValue.setVisible(true);
		        	return;
	        	}
	        	
	            charges += baseFare;
	        	if(checkBox1.isSelected()) //extra luggage
	        	{
	        		extra_luggage = 10.00;
	        		charges += extra_luggage;
	        	}
	        	
	        	if(checkBox2.isSelected()) //pets
	        	{
	        		pets = 6.00;
	        		charges += pets;
	        	}
	        	
	        	if(checkBox3.isSelected()) //use 407
	        	{
	        		use407 = 0.25;
	        		charges += use407;
	        	}
	        	
	        	tax = charges * 0.13; //charges include base fare
	        	
	        	if(checkBox4.isSelected()) //tip
	        	{     		
	        		tip = (charges + tax) * 0.15;	
	        	}
	        	
	        	totalPrice = charges + tax + tip;
	        	
	        	lbl_totalFair.setVisible(true);
	        	lbl_totalFairValue.setText("$"+df.format(totalPrice));
	        	lbl_totalFairValue.setVisible(true);
	        	}
	        
	    });
		
	}

}
